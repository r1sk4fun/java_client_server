import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class Server {
    public static void main(String[] args) {
        int portNumber = 8080;
        int size = 10;
        int min = 1;
        int max = 100;

        // Initialize the array with random numbers
        int[] randomArray = generateRandomIntArray(size, min, max);

        try {
            // Create a server socket
            ServerSocket serverSocket = new ServerSocket(portNumber);

            System.out.println("Server started. Waiting for clients...");

            while (true) {
                // Accept client connection
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress());

                // Create input and output streams for reading/writing data
                BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                PrintWriter output = new PrintWriter(clientSocket.getOutputStream(), true);

                // Read the number from the client
                int targetNumber = Integer.parseInt(input.readLine());

                // Find the closest number in the array
                int closestNumber = findClosestNumber(randomArray, targetNumber);

                // Find the serial number of the closest number
                int serialNumber = findSerialNumber(randomArray, closestNumber);
                
                // Send the random array back to the client
                for (int i = 0; i < randomArray.length; i++) {
                    output.print(randomArray[i] + " ");
                }

                // blank line
                output.println();

                // Send the serial number back to the client
                output.println(serialNumber);

                // Close the client socket and input/output streams
                clientSocket.close();
                input.close();
                output.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static int[] generateRandomIntArray(int size, int min, int max) {
        int[] array = new int[size];

        for (int i = 0; i < size; i++) {
            array[i] = (int) (Math.random() * (max - min + 1)) + min;
        }

        return array;
    }

    // Method to find the closest number in the array
    private static int findClosestNumber(int[] array, int target) {
        int closestNumber = array[0];
        int minDifference = Math.abs(target - closestNumber);

        for (int i = 1; i < array.length; i++) {
            int difference = Math.abs(target - array[i]);
            if (difference < minDifference) {
                minDifference = difference;
                closestNumber = array[i];
            }
        }

        return closestNumber;
    }

    // Method to find the serial number of a number in the array
    private static int findSerialNumber(int[] array, int number) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == number) {
                return i;
            }
        }
        return -1; // Number not found in the array
    }
}
