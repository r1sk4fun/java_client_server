import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    public static void main(String[] args) {
        String serverAddress = "localhost";
        int serverPort = 8080;

        try {
            // Create a socket and connect to the server
            Socket socket = new Socket(serverAddress, serverPort);

            // Create input and output streams for reading/writing data
            BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

            // Create a separate thread to continuously read from the keyboard
            Thread keyboardThread = new Thread(() -> {
                try {
                    BufferedReader keyboardInput = new BufferedReader(new InputStreamReader(System.in));
                    String userInput;

                    while ((userInput = keyboardInput.readLine()) != null) {
                        // Send the user input to the server
                        output.println(userInput);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            keyboardThread.start();

            // Read server responses and print them to the console
            String serverResponse;
            while ((serverResponse = input.readLine()) != null) {
                System.out.println(serverResponse);
            }

            // Close the socket and input/output streams
            socket.close();
            input.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
